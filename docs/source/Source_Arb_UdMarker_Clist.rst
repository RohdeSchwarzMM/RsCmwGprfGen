Clist
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:ARB:UDMarker:CLISt

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:ARB:UDMarker:CLISt



.. autoclass:: RsCmwGprfGen.Implementations.Source.Arb.UdMarker.Clist.ClistCls
	:members:
	:undoc-members:
	:noindex: