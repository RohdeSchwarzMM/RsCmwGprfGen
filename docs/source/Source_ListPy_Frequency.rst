Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:LIST:FREQuency
	single: SOURce:GPRF:GENerator<Instance>:LIST:FREQuency:ALL

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:LIST:FREQuency
	SOURce:GPRF:GENerator<Instance>:LIST:FREQuency:ALL



.. autoclass:: RsCmwGprfGen.Implementations.Source.ListPy.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: