Delays
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:MARKer:DELays

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:MARKer:DELays



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.Marker.Delays.DelaysCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.sequencer.marker.delays.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Sequencer_Marker_Delays_All.rst