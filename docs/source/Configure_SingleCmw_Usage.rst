Usage
----------------------------------------





.. autoclass:: RsCmwGprfGen.Implementations.Configure.SingleCmw.Usage.UsageCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.singleCmw.usage.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_SingleCmw_Usage_Tx.rst