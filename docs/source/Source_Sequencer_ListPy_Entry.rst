Entry
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ENTRy:DELete

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ENTRy:DELete



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.ListPy.Entry.EntryCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.sequencer.listPy.entry.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Sequencer_ListPy_Entry_Call.rst
	Source_Sequencer_ListPy_Entry_Insert.rst
	Source_Sequencer_ListPy_Entry_Mdown.rst
	Source_Sequencer_ListPy_Entry_Mup.rst