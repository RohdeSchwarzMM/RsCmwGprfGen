Dtone
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:DTONe:RATio

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:DTONe:RATio



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.Dtone.DtoneCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.sequencer.dtone.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Sequencer_Dtone_Ofrequency.rst