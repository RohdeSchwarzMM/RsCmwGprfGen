Execute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:GENerator<Instance>:SEQuencer:MANual:EXECute

.. code-block:: python

	TRIGger:GPRF:GENerator<Instance>:SEQuencer:MANual:EXECute



.. autoclass:: RsCmwGprfGen.Implementations.Trigger.Sequencer.Manual.Execute.ExecuteCls
	:members:
	:undoc-members:
	:noindex: