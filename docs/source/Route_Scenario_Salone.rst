Salone
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:GPRF:GENerator<Instance>:SCENario:SALone

.. code-block:: python

	ROUTe:GPRF:GENerator<Instance>:SCENario:SALone



.. autoclass:: RsCmwGprfGen.Implementations.Route.Scenario.Salone.SaloneCls
	:members:
	:undoc-members:
	:noindex: