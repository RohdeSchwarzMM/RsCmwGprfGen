File
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:ARB:FILE
	single: SOURce:GPRF:GENerator<Instance>:ARB:FILE:DATE
	single: SOURce:GPRF:GENerator<Instance>:ARB:FILE:VERSion
	single: SOURce:GPRF:GENerator<Instance>:ARB:FILE:OPTion

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:ARB:FILE
	SOURce:GPRF:GENerator<Instance>:ARB:FILE:DATE
	SOURce:GPRF:GENerator<Instance>:ARB:FILE:VERSion
	SOURce:GPRF:GENerator<Instance>:ARB:FILE:OPTion



.. autoclass:: RsCmwGprfGen.Implementations.Source.Arb.File.FileCls
	:members:
	:undoc-members:
	:noindex: