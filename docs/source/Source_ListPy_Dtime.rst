Dtime
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:LIST:DTIMe
	single: SOURce:GPRF:GENerator<Instance>:LIST:DTIMe:ALL

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:LIST:DTIMe
	SOURce:GPRF:GENerator<Instance>:LIST:DTIMe:ALL



.. autoclass:: RsCmwGprfGen.Implementations.Source.ListPy.Dtime.DtimeCls
	:members:
	:undoc-members:
	:noindex: