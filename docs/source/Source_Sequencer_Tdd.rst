Tdd
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:TDD:MODE

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:TDD:MODE



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.Tdd.TddCls
	:members:
	:undoc-members:
	:noindex: