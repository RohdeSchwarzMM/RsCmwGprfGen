Tx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:LIST:CMWS:USAGe:TX

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:LIST:CMWS:USAGe:TX



.. autoclass:: RsCmwGprfGen.Implementations.Source.ListPy.SingleCmw.Usage.Tx.TxCls
	:members:
	:undoc-members:
	:noindex: