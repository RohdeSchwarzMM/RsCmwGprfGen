Segments
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:ARB:SEGMents:NEXT
	single: SOURce:GPRF:GENerator<Instance>:ARB:SEGMents:CURRent

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:ARB:SEGMents:NEXT
	SOURce:GPRF:GENerator<Instance>:ARB:SEGMents:CURRent



.. autoclass:: RsCmwGprfGen.Implementations.Source.Arb.Segments.SegmentsCls
	:members:
	:undoc-members:
	:noindex: