Arb
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:ARB:FOFFset
	single: SOURce:GPRF:GENerator<Instance>:ARB:SCOunt
	single: SOURce:GPRF:GENerator<Instance>:ARB:ASAMples
	single: SOURce:GPRF:GENerator<Instance>:ARB:REPetition
	single: SOURce:GPRF:GENerator<Instance>:ARB:CYCLes
	single: SOURce:GPRF:GENerator<Instance>:ARB:POFFset
	single: SOURce:GPRF:GENerator<Instance>:ARB:CRATe
	single: SOURce:GPRF:GENerator<Instance>:ARB:LOFFset
	single: SOURce:GPRF:GENerator<Instance>:ARB:CRCProtect
	single: SOURce:GPRF:GENerator<Instance>:ARB:STATus

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:ARB:FOFFset
	SOURce:GPRF:GENerator<Instance>:ARB:SCOunt
	SOURce:GPRF:GENerator<Instance>:ARB:ASAMples
	SOURce:GPRF:GENerator<Instance>:ARB:REPetition
	SOURce:GPRF:GENerator<Instance>:ARB:CYCLes
	SOURce:GPRF:GENerator<Instance>:ARB:POFFset
	SOURce:GPRF:GENerator<Instance>:ARB:CRATe
	SOURce:GPRF:GENerator<Instance>:ARB:LOFFset
	SOURce:GPRF:GENerator<Instance>:ARB:CRCProtect
	SOURce:GPRF:GENerator<Instance>:ARB:STATus



.. autoclass:: RsCmwGprfGen.Implementations.Source.Arb.ArbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.arb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Arb_File.rst
	Source_Arb_Marker.rst
	Source_Arb_Msegment.rst
	Source_Arb_Samples.rst
	Source_Arb_Segments.rst
	Source_Arb_UdMarker.rst