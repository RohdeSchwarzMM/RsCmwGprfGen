ListPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:LIST:AINDex
	single: SOURce:GPRF:GENerator<Instance>:LIST:FILL
	single: SOURce:GPRF:GENerator<Instance>:LIST:GOTO
	single: SOURce:GPRF:GENerator<Instance>:LIST:REPetition
	single: SOURce:GPRF:GENerator<Instance>:LIST:STARt
	single: SOURce:GPRF:GENerator<Instance>:LIST:STOP
	single: SOURce:GPRF:GENerator<Instance>:LIST:COUNt
	single: SOURce:GPRF:GENerator<Instance>:LIST

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:LIST:AINDex
	SOURce:GPRF:GENerator<Instance>:LIST:FILL
	SOURce:GPRF:GENerator<Instance>:LIST:GOTO
	SOURce:GPRF:GENerator<Instance>:LIST:REPetition
	SOURce:GPRF:GENerator<Instance>:LIST:STARt
	SOURce:GPRF:GENerator<Instance>:LIST:STOP
	SOURce:GPRF:GENerator<Instance>:LIST:COUNt
	SOURce:GPRF:GENerator<Instance>:LIST



.. autoclass:: RsCmwGprfGen.Implementations.Source.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_ListPy_Dgain.rst
	Source_ListPy_Dtime.rst
	Source_ListPy_Esingle.rst
	Source_ListPy_Frequency.rst
	Source_ListPy_Increment.rst
	Source_ListPy_Irepetition.rst
	Source_ListPy_Modulation.rst
	Source_ListPy_Reenabling.rst
	Source_ListPy_RfLevel.rst
	Source_ListPy_Rlist.rst
	Source_ListPy_SingleCmw.rst
	Source_ListPy_Slist.rst
	Source_ListPy_Sstop.rst