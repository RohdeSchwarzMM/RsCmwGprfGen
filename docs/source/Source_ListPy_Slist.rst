Slist
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:LIST:SLISt

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:LIST:SLISt



.. autoclass:: RsCmwGprfGen.Implementations.Source.ListPy.Slist.SlistCls
	:members:
	:undoc-members:
	:noindex: