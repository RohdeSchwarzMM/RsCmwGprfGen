Roption
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:ROPTion:ALL
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:ROPTion

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:ROPTion:ALL
	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:ROPTion



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.Apool.Roption.RoptionCls
	:members:
	:undoc-members:
	:noindex: