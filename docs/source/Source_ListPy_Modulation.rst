Modulation
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:LIST:MODulation
	single: SOURce:GPRF:GENerator<Instance>:LIST:MODulation:ALL

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:LIST:MODulation
	SOURce:GPRF:GENerator<Instance>:LIST:MODulation:ALL



.. autoclass:: RsCmwGprfGen.Implementations.Source.ListPy.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex: