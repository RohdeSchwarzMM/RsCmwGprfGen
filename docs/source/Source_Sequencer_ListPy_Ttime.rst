Ttime
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:TTIMe
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:TTIMe:ALL

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:TTIMe
	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:TTIMe:ALL



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.ListPy.Ttime.TtimeCls
	:members:
	:undoc-members:
	:noindex: