Delays
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:ARB:MARKer:DELays

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:ARB:MARKer:DELays



.. autoclass:: RsCmwGprfGen.Implementations.Source.Arb.Marker.Delays.DelaysCls
	:members:
	:undoc-members:
	:noindex: