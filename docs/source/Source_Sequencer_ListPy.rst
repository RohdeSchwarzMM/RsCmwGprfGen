ListPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:CREate
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:INDex
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:MINDex

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:CREate
	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:INDex
	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:MINDex



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.sequencer.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Sequencer_ListPy_Acycles.rst
	Source_Sequencer_ListPy_Dgain.rst
	Source_Sequencer_ListPy_Dtime.rst
	Source_Sequencer_ListPy_Entry.rst
	Source_Sequencer_ListPy_Fill.rst
	Source_Sequencer_ListPy_Frequency.rst
	Source_Sequencer_ListPy_Itransition.rst
	Source_Sequencer_ListPy_Lincrement.rst
	Source_Sequencer_ListPy_Lrms.rst
	Source_Sequencer_ListPy_Signal.rst
	Source_Sequencer_ListPy_SingleCmw.rst
	Source_Sequencer_ListPy_Spath.rst
	Source_Sequencer_ListPy_SymbolRate.rst
	Source_Sequencer_ListPy_Ttime.rst