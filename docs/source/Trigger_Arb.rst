Arb
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:GPRF:GENerator<Instance>:ARB:SOURce
	single: TRIGger:GPRF:GENerator<Instance>:ARB:DELay
	single: TRIGger:GPRF:GENerator<Instance>:ARB:SLOPe
	single: TRIGger:GPRF:GENerator<Instance>:ARB:RETRigger
	single: TRIGger:GPRF:GENerator<Instance>:ARB:AUTostart

.. code-block:: python

	TRIGger:GPRF:GENerator<Instance>:ARB:SOURce
	TRIGger:GPRF:GENerator<Instance>:ARB:DELay
	TRIGger:GPRF:GENerator<Instance>:ARB:SLOPe
	TRIGger:GPRF:GENerator<Instance>:ARB:RETRigger
	TRIGger:GPRF:GENerator<Instance>:ARB:AUTostart



.. autoclass:: RsCmwGprfGen.Implementations.Trigger.Arb.ArbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.arb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Arb_Catalog.rst
	Trigger_Arb_Manual.rst
	Trigger_Arb_Segments.rst