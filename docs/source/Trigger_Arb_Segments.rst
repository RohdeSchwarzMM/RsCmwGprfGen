Segments
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:GENerator<Instance>:ARB:SEGMents:MODE

.. code-block:: python

	TRIGger:GPRF:GENerator<Instance>:ARB:SEGMents:MODE



.. autoclass:: RsCmwGprfGen.Implementations.Trigger.Arb.Segments.SegmentsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.arb.segments.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Arb_Segments_Manual.rst