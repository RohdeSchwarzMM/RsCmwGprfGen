Samples
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:SAMPles:ALL
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:SAMPles

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:SAMPles:ALL
	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:SAMPles



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.Apool.Samples.SamplesCls
	:members:
	:undoc-members:
	:noindex: