Signal
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SIGNal:CATalog
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SIGNal
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SIGNal:ALL

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SIGNal:CATalog
	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SIGNal
	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SIGNal:ALL



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.ListPy.Signal.SignalCls
	:members:
	:undoc-members:
	:noindex: