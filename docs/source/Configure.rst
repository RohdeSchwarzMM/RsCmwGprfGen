Configure
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:GENerator<Instance>:TYPE

.. code-block:: python

	CONFigure:GPRF:GENerator<Instance>:TYPE



.. autoclass:: RsCmwGprfGen.Implementations.Configure.ConfigureCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_SingleCmw.rst
	Configure_Spath.rst