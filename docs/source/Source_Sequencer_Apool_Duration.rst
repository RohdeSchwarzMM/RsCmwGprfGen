Duration
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:DURation:ALL
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:DURation

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:DURation:ALL
	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:DURation



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.Apool.Duration.DurationCls
	:members:
	:undoc-members:
	:noindex: