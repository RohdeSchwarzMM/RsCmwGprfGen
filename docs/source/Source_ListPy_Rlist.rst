Rlist
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:LIST:RLISt

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:LIST:RLISt



.. autoclass:: RsCmwGprfGen.Implementations.Source.ListPy.Rlist.RlistCls
	:members:
	:undoc-members:
	:noindex: