Samples
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:ARB:SAMPles

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:ARB:SAMPles



.. autoclass:: RsCmwGprfGen.Implementations.Source.Arb.Samples.SamplesCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.arb.samples.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Arb_Samples_Range.rst