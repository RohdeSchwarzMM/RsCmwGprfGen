All
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:GENerator<Instance>:CMWS:USAGe:TX:ALL

.. code-block:: python

	CONFigure:GPRF:GENerator<Instance>:CMWS:USAGe:TX:ALL



.. autoclass:: RsCmwGprfGen.Implementations.Configure.SingleCmw.Usage.Tx.All.AllCls
	:members:
	:undoc-members:
	:noindex: