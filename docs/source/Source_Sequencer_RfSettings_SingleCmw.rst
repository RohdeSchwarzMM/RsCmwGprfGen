SingleCmw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:RFSettings:CMWS:CSET

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:RFSettings:CMWS:CSET



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.RfSettings.SingleCmw.SingleCmwCls
	:members:
	:undoc-members:
	:noindex: