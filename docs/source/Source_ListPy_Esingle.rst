Esingle
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:LIST:ESINgle

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:LIST:ESINgle



.. autoclass:: RsCmwGprfGen.Implementations.Source.ListPy.Esingle.EsingleCls
	:members:
	:undoc-members:
	:noindex: