Wmarker<Marker>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.source.sequencer.wmarker.repcap_marker_get()
	driver.source.sequencer.wmarker.repcap_marker_set(repcap.Marker.Nr1)





.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.Wmarker.WmarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.sequencer.wmarker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Sequencer_Wmarker_Delay.rst