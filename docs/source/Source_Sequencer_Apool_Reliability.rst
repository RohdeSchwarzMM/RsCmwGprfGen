Reliability
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:RELiability

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:RELiability



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.Apool.Reliability.ReliabilityCls
	:members:
	:undoc-members:
	:noindex: