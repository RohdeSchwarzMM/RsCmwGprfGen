Mdown
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ENTRy:MDOWn

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ENTRy:MDOWn



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.ListPy.Entry.Mdown.MdownCls
	:members:
	:undoc-members:
	:noindex: