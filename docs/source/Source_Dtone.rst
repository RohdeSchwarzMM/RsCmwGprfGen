Dtone
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:DTONe:RATio

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:DTONe:RATio



.. autoclass:: RsCmwGprfGen.Implementations.Source.Dtone.DtoneCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.dtone.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Dtone_Level.rst
	Source_Dtone_Ofrequency.rst