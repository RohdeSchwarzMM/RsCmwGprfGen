RepCaps
=========

Instance (Global)
----------------------------------------------------

.. code-block:: python

	# Setting:
	driver.repcap_instance_set(repcap.Instance.Inst1)
	# Range:
	Inst1 .. Inst32
	# All values (32x):
	Inst1 | Inst2 | Inst3 | Inst4 | Inst5 | Inst6 | Inst7 | Inst8
	Inst9 | Inst10 | Inst11 | Inst12 | Inst13 | Inst14 | Inst15 | Inst16
	Inst17 | Inst18 | Inst19 | Inst20 | Inst21 | Inst22 | Inst23 | Inst24
	Inst25 | Inst26 | Inst27 | Inst28 | Inst29 | Inst30 | Inst31 | Inst32

Bench
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Bench.Nr1
	# Range:
	Nr1 .. Nr20
	# All values (20x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20

FrequencySource
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.FrequencySource.Src1
	# Values (2x):
	Src1 | Src2

LevelSource
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.LevelSource.Src1
	# Values (2x):
	Src1 | Src2

Marker
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Marker.Nr1
	# Values (4x):
	Nr1 | Nr2 | Nr3 | Nr4

