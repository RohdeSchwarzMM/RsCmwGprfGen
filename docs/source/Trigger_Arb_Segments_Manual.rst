Manual
----------------------------------------





.. autoclass:: RsCmwGprfGen.Implementations.Trigger.Arb.Segments.Manual.ManualCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.arb.segments.manual.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Arb_Segments_Manual_Execute.rst