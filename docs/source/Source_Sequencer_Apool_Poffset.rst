Poffset
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:POFFset:ALL
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:POFFset

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:POFFset:ALL
	SOURce:GPRF:GENerator<Instance>:SEQuencer:APOol:POFFset



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.Apool.Poffset.PoffsetCls
	:members:
	:undoc-members:
	:noindex: