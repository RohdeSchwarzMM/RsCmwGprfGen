Spath
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:GENerator<Instance>:SPATh:BCSWitch

.. code-block:: python

	CONFigure:GPRF:GENerator<Instance>:SPATh:BCSWitch



.. autoclass:: RsCmwGprfGen.Implementations.Configure.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.spath.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Spath_Usage.rst