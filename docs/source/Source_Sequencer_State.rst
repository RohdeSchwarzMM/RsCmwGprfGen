State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:STATe

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:STATe



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.sequencer.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Sequencer_State_All.rst