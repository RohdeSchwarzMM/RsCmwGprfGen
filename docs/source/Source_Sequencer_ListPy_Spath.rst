Spath
----------------------------------------





.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.ListPy.Spath.SpathCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.sequencer.listPy.spath.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Sequencer_ListPy_Spath_Usage.rst