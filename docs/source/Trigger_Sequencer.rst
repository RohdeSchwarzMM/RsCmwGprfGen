Sequencer
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:GENerator<Instance>:SEQuencer:TOUT

.. code-block:: python

	TRIGger:GPRF:GENerator<Instance>:SEQuencer:TOUT



.. autoclass:: RsCmwGprfGen.Implementations.Trigger.Sequencer.SequencerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.sequencer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Sequencer_IsMeas.rst
	Trigger_Sequencer_IsTrigger.rst
	Trigger_Sequencer_Manual.rst