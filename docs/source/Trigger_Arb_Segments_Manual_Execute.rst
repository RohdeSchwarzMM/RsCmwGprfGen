Execute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:GENerator<Instance>:ARB:SEGMents:MANual:EXECute

.. code-block:: python

	TRIGger:GPRF:GENerator<Instance>:ARB:SEGMents:MANual:EXECute



.. autoclass:: RsCmwGprfGen.Implementations.Trigger.Arb.Segments.Manual.Execute.ExecuteCls
	:members:
	:undoc-members:
	:noindex: