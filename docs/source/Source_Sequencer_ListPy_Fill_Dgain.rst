Dgain
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:DGAin:SVALue
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:DGAin:INCRement
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:DGAin:KEEP

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:DGAin:SVALue
	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:DGAin:INCRement
	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:DGAin:KEEP



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.ListPy.Fill.Dgain.DgainCls
	:members:
	:undoc-members:
	:noindex: