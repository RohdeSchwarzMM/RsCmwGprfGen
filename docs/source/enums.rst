Enums
=========

ArbFile
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ArbFile.ABSPath
	# All values (3x):
	ABSPath | DEF | TRUTh

ArbSegmentsMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ArbSegmentsMode.AUTO
	# All values (3x):
	AUTO | CONTinuous | CSEamless

BasebandMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BasebandMode.ARB
	# All values (3x):
	ARB | CW | DTONe

GeneratorState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GeneratorState.ADJusted
	# All values (8x):
	ADJusted | AUTonomous | COUPled | INValid | OFF | ON | PENDing | RDY

IncTransition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IncTransition.IMMediate
	# All values (6x):
	IMMediate | RMARker | WMA1 | WMA2 | WMA3 | WMA4

InstrumentType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InstrumentType.PROTocol
	# All values (2x):
	PROTocol | SIGNaling

ListIncrement
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ListIncrement.ACYCles
	# All values (5x):
	ACYCles | DTIMe | MEASurement | TRIGger | USER

ParameterSetMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ParameterSetMode.GLOBal
	# All values (2x):
	GLOBal | LIST

Range
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Range.FULL
	# All values (2x):
	FULL | SUB

RepeatMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RepeatMode.CONTinuous
	# All values (2x):
	CONTinuous | SINGle

Scenario
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Scenario.IQOut
	# All values (3x):
	IQOut | NAV | SALone

SignalSlope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalSlope.FEDGe
	# All values (2x):
	FEDGe | REDGe

TargetMainState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetMainState.OFF
	# All values (3x):
	OFF | RUN | STOP

TargetSyncState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetSyncState.ADJusted
	# All values (2x):
	ADJusted | PENDing

TransferMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TransferMode.ENABlemode
	# All values (2x):
	ENABlemode | REQuestmode

TxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConnector.I12O
	# Last value:
	value = enums.TxConnector.RH18
	# All values (86x):
	I12O | I14O | I16O | I18O | I22O | I24O | I26O | I28O
	I32O | I34O | I36O | I38O | I42O | I44O | I46O | I48O
	IFO1 | IFO2 | IFO3 | IFO4 | IFO5 | IFO6 | IQ2O | IQ4O
	IQ6O | IQ8O | R10D | R118 | R1183 | R1184 | R11C | R11D
	R11O | R11O3 | R11O4 | R12C | R12D | R13C | R13O | R14C
	R214 | R218 | R21C | R21O | R22C | R23C | R23O | R24C
	R258 | R318 | R31C | R31O | R32C | R33C | R33O | R34C
	R418 | R41C | R41O | R42C | R43C | R43O | R44C | RA18
	RB14 | RB18 | RC18 | RD18 | RE18 | RF18 | RF1C | RF1O
	RF2C | RF3C | RF3O | RF4C | RF5C | RF6C | RF7C | RF8C
	RF9C | RFAC | RFAO | RFBC | RG18 | RH18

TxConnectorBench
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConnectorBench.R118
	# Last value:
	value = enums.TxConnectorBench.RH18
	# All values (15x):
	R118 | R214 | R218 | R258 | R318 | R418 | RA18 | RB14
	RB18 | RC18 | RD18 | RE18 | RF18 | RG18 | RH18

TxConnectorCmws
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConnectorCmws.R11
	# Last value:
	value = enums.TxConnectorCmws.RH8
	# All values (96x):
	R11 | R12 | R13 | R14 | R15 | R16 | R17 | R18
	R21 | R22 | R23 | R24 | R25 | R26 | R27 | R28
	R31 | R32 | R33 | R34 | R35 | R36 | R37 | R38
	R41 | R42 | R43 | R44 | R45 | R46 | R47 | R48
	RA1 | RA2 | RA3 | RA4 | RA5 | RA6 | RA7 | RA8
	RB1 | RB2 | RB3 | RB4 | RB5 | RB6 | RB7 | RB8
	RC1 | RC2 | RC3 | RC4 | RC5 | RC6 | RC7 | RC8
	RD1 | RD2 | RD3 | RD4 | RD5 | RD6 | RD7 | RD8
	RE1 | RE2 | RE3 | RE4 | RE5 | RE6 | RE7 | RE8
	RF1 | RF2 | RF3 | RF4 | RF5 | RF6 | RF7 | RF8
	RG1 | RG2 | RG3 | RG4 | RG5 | RG6 | RG7 | RG8
	RH1 | RH2 | RH3 | RH4 | RH5 | RH6 | RH7 | RH8

TxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConverter.ITX1
	# Last value:
	value = enums.TxConverter.TX44
	# All values (40x):
	ITX1 | ITX11 | ITX12 | ITX13 | ITX14 | ITX2 | ITX21 | ITX22
	ITX23 | ITX24 | ITX3 | ITX31 | ITX32 | ITX33 | ITX34 | ITX4
	ITX41 | ITX42 | ITX43 | ITX44 | TX1 | TX11 | TX12 | TX13
	TX14 | TX2 | TX21 | TX22 | TX23 | TX24 | TX3 | TX31
	TX32 | TX33 | TX34 | TX4 | TX41 | TX42 | TX43 | TX44

YesNoStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.YesNoStatus.NO
	# All values (2x):
	NO | YES

