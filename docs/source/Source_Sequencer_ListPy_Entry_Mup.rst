Mup
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ENTRy:MUP

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ENTRy:MUP



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.ListPy.Entry.Mup.MupCls
	:members:
	:undoc-members:
	:noindex: