Sstop
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:LIST:SSTop

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:LIST:SSTop



.. autoclass:: RsCmwGprfGen.Implementations.Source.ListPy.Sstop.SstopCls
	:members:
	:undoc-members:
	:noindex: