Tx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:GENerator<Instance>:CMWS:USAGe:TX

.. code-block:: python

	CONFigure:GPRF:GENerator<Instance>:CMWS:USAGe:TX



.. autoclass:: RsCmwGprfGen.Implementations.Configure.SingleCmw.Usage.Tx.TxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.singleCmw.usage.tx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_SingleCmw_Usage_Tx_All.rst