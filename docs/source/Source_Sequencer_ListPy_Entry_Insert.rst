Insert
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ENTRy:INSert

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ENTRy:INSert



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.ListPy.Entry.Insert.InsertCls
	:members:
	:undoc-members:
	:noindex: