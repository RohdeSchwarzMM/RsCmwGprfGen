Level<LevelSource>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Src1 .. Src2
	rc = driver.source.dtone.level.repcap_levelSource_get()
	driver.source.dtone.level.repcap_levelSource_set(repcap.LevelSource.Src1)



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:DTONe:LEVel<source>

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:DTONe:LEVel<source>



.. autoclass:: RsCmwGprfGen.Implementations.Source.Dtone.Level.LevelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.dtone.level.clone()