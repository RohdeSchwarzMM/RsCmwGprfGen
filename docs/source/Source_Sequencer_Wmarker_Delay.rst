Delay
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:WMARker<no>:DELay

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:WMARker<no>:DELay



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.Wmarker.Delay.DelayCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.sequencer.wmarker.delay.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Sequencer_Wmarker_Delay_All.rst