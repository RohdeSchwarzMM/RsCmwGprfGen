Irepetition
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:LIST:IREPetition
	single: SOURce:GPRF:GENerator<Instance>:LIST:IREPetition:ALL

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:LIST:IREPetition
	SOURce:GPRF:GENerator<Instance>:LIST:IREPetition:ALL



.. autoclass:: RsCmwGprfGen.Implementations.Source.ListPy.Irepetition.IrepetitionCls
	:members:
	:undoc-members:
	:noindex: