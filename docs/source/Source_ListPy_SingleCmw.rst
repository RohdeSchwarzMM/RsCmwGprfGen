SingleCmw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:LIST:CMWS:CSET

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:LIST:CMWS:CSET



.. autoclass:: RsCmwGprfGen.Implementations.Source.ListPy.SingleCmw.SingleCmwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.listPy.singleCmw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_ListPy_SingleCmw_Usage.rst