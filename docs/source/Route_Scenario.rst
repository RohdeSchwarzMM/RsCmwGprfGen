Scenario
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:GPRF:GENerator<Instance>:SCENario

.. code-block:: python

	ROUTe:GPRF:GENerator<Instance>:SCENario



.. autoclass:: RsCmwGprfGen.Implementations.Route.Scenario.ScenarioCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_IqOut.rst
	Route_Scenario_Salone.rst