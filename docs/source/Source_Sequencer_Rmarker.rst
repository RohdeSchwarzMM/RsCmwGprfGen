Rmarker
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:RMARker:DELay

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:RMARker:DELay



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.Rmarker.RmarkerCls
	:members:
	:undoc-members:
	:noindex: