Source
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:BBMode

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:BBMode



.. autoclass:: RsCmwGprfGen.Implementations.Source.SourceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Arb.rst
	Source_Dtone.rst
	Source_IqSettings.rst
	Source_ListPy.rst
	Source_RfSettings.rst
	Source_Sequencer.rst
	Source_State.rst