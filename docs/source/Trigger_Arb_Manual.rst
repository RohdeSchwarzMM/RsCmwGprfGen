Manual
----------------------------------------





.. autoclass:: RsCmwGprfGen.Implementations.Trigger.Arb.Manual.ManualCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.arb.manual.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Arb_Manual_Execute.rst