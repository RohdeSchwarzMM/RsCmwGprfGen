Execute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:GENerator<Instance>:ARB:MANual:EXECute

.. code-block:: python

	TRIGger:GPRF:GENerator<Instance>:ARB:MANual:EXECute



.. autoclass:: RsCmwGprfGen.Implementations.Trigger.Arb.Manual.Execute.ExecuteCls
	:members:
	:undoc-members:
	:noindex: