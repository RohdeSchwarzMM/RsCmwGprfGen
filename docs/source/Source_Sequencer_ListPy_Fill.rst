Fill
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:SINDex
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:RANGe

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:SINDex
	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:FILL:RANGe



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.ListPy.Fill.FillCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.sequencer.listPy.fill.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Sequencer_ListPy_Fill_Apply.rst
	Source_Sequencer_ListPy_Fill_Dgain.rst
	Source_Sequencer_ListPy_Fill_Frequency.rst
	Source_Sequencer_ListPy_Fill_Lrms.rst