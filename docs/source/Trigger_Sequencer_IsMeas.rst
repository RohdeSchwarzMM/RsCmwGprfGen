IsMeas
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:GPRF:GENerator<Instance>:SEQuencer:ISMeas:CATalog
	single: TRIGger:GPRF:GENerator<Instance>:SEQuencer:ISMeas:SOURce

.. code-block:: python

	TRIGger:GPRF:GENerator<Instance>:SEQuencer:ISMeas:CATalog
	TRIGger:GPRF:GENerator<Instance>:SEQuencer:ISMeas:SOURce



.. autoclass:: RsCmwGprfGen.Implementations.Trigger.Sequencer.IsMeas.IsMeasCls
	:members:
	:undoc-members:
	:noindex: