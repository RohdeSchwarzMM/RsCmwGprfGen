Bench<Bench>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr20
	rc = driver.configure.spath.usage.bench.repcap_bench_get()
	driver.configure.spath.usage.bench.repcap_bench_set(repcap.Bench.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:GENerator<Instance>:SPATh:USAGe:BENCh<nr>

.. code-block:: python

	CONFigure:GPRF:GENerator<Instance>:SPATh:USAGe:BENCh<nr>



.. autoclass:: RsCmwGprfGen.Implementations.Configure.Spath.Usage.Bench.BenchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.spath.usage.bench.clone()