Itransition
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ITRansition
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ITRansition:ALL

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ITRansition
	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:ITRansition:ALL



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.ListPy.Itransition.ItransitionCls
	:members:
	:undoc-members:
	:noindex: