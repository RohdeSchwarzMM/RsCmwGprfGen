Usage
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:GENerator<Instance>:SPATh:USAGe

.. code-block:: python

	CONFigure:GPRF:GENerator<Instance>:SPATh:USAGe



.. autoclass:: RsCmwGprfGen.Implementations.Configure.Spath.Usage.UsageCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.spath.usage.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Spath_Usage_Bench.rst