Usage
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SPATh:USAGe

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:SEQuencer:LIST:SPATh:USAGe



.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.ListPy.Spath.Usage.UsageCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.sequencer.listPy.spath.usage.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Sequencer_ListPy_Spath_Usage_Bench.rst