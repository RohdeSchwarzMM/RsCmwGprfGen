IsTrigger
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:GPRF:GENerator<Instance>:SEQuencer:ISTRigger:CATalog
	single: TRIGger:GPRF:GENerator<Instance>:SEQuencer:ISTRigger:SOURce

.. code-block:: python

	TRIGger:GPRF:GENerator<Instance>:SEQuencer:ISTRigger:CATalog
	TRIGger:GPRF:GENerator<Instance>:SEQuencer:ISTRigger:SOURce



.. autoclass:: RsCmwGprfGen.Implementations.Trigger.Sequencer.IsTrigger.IsTriggerCls
	:members:
	:undoc-members:
	:noindex: