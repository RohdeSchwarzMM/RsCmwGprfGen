RfSettings
----------------------------------------





.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.sequencer.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Sequencer_RfSettings_SingleCmw.rst
	Source_Sequencer_RfSettings_Spath.rst