Range
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce:GPRF:GENerator<Instance>:ARB:SAMPles:RANGe

.. code-block:: python

	SOURce:GPRF:GENerator<Instance>:ARB:SAMPles:RANGe



.. autoclass:: RsCmwGprfGen.Implementations.Source.Arb.Samples.Range.RangeCls
	:members:
	:undoc-members:
	:noindex: