Marker
----------------------------------------





.. autoclass:: RsCmwGprfGen.Implementations.Source.Sequencer.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.sequencer.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Sequencer_Marker_Delays.rst