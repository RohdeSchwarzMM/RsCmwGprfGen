RsCmwGprfGen API Structure
========================================


.. rubric:: Global RepCaps

.. code-block:: python
	
	driver = RsCmwGprfGen('TCPIP::192.168.2.101::hislip0')
	# Instance range: Inst1 .. Inst32
	rc = driver.repcap_instance_get()
	driver.repcap_instance_set(repcap.Instance.Inst1)

.. autoclass:: RsCmwGprfGen.RsCmwGprfGen
	:members:
	:undoc-members:
	:noindex:

.. rubric:: Subgroups

.. toctree::
	:maxdepth: 6
	:glob:

	Configure.rst
	Route.rst
	Source.rst
	Trigger.rst